<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Jonathan Kashongwe',
            'email' => 'jkashongwe@yahoo.fr',
            'password' => bcrypt('password')
        ]);
    }
}
